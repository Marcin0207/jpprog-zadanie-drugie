package zadaniezdata;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("podaj date najbliższych zajęc!");
        String lessonDateS = sc.nextLine();
        LocalDate lessonDate = LocalDate.parse(lessonDateS);
        System.out.println(lessonDate);

        LocalDate currentDate = LocalDate.now();
        System.out.println(currentDate);

        Period intervalPeriod = Period.between(currentDate, lessonDate);



        System.out.println("Difference of days: " + intervalPeriod.getDays());
        System.out.println("Difference of months: " + intervalPeriod.getMonths());
        System.out.println("Difference of years: " + intervalPeriod.getYears());

        long noOfDaysBetween = ChronoUnit.DAYS.between(currentDate, lessonDate);
        System.out.println(noOfDaysBetween);

        System.out.println("Difference of days only: "+noOfDaysBetween);



    }

}
